package com.macgal.brewingcalculator;

import android.content.Context;
import android.graphics.Typeface;
import android.view.Gravity;
import android.widget.TableRow;
import android.widget.TextView;

public class Utils {

	public Utils() {
		// TODO Auto-generated constructor stub
	}

	
	 /**
	  * Convierte de temperatura en Celsius a Farenheit
	 * @param temperatura en celsius
	 * @return
	 */
	public static double celsiusToFahrenheit(double celsius)
     {
            return ((celsius * 1.8) + 32.0);
     }
	
	
	public static double litersToGallons(double liters)
    {
        return (liters * 0.264172052);
    }
	
	public static TableRow GetTableRow(Context context, String[] pItems, boolean isHeader) {
		
		TableRow wRow = new TableRow(context);

		
		TextView[] txtCols = new TextView[pItems.length];
		
		for(int idx=0; idx<pItems.length;idx++){
			txtCols[idx] = new TextView(context);
			txtCols[idx].setText(pItems[idx]);
			
			if(idx>0)
				txtCols[idx].setGravity(Gravity.RIGHT);
			
			if(isHeader)
				txtCols[idx].setTypeface(Typeface.DEFAULT_BOLD);
			else
				txtCols[idx].setTypeface(Typeface.DEFAULT);
			
			wRow.addView(txtCols[idx]);
		} 

		return wRow;
	}
	
	
}
