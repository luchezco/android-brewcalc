/**
 * 
 */
package com.macgal.brewingcalculator.ui;

import com.macgal.brewingcalculator.Calcs;
import com.macgal.brewingcalculator.R;
import com.macgal.brewingcalculator.Utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.TableLayout;

/**
 * @author luchezco
 *
 */
public class WortMix extends Activity {

	private static WortMix staticWortMix;
	
	/** Called when the activity is first created. */
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setTitle(getText(R.string.MenuWortMix));
		setContentView(R.layout.wortmix_detail);
		staticWortMix=this;
		clearFields();
	}
	
	private void clearFields(){
		((EditText)findViewById(R.id.wortMix_txtfirstWorthLiters)).setText("");
		((EditText)findViewById(R.id.wortMix_txtfirstWorthLiters)).requestFocus(MODE_PRIVATE);
		((EditText)findViewById(R.id.wortMix_txtfirstWortGravity)).setText("");
		((EditText)findViewById(R.id.wortMix_txtsecondWorthLiters)).setText("");
		((EditText)findViewById(R.id.wortMix_txtsecondWortGravity)).setText("");
		
		TableLayout wTableResults = (TableLayout) findViewById(R.id.wortMix_Results);
		wTableResults.removeAllViews();
	}
	
	private boolean validateFields()
	{
		boolean isValid=true;
		
		try{
			double firstWortLiters =Double.parseDouble(((EditText)findViewById(R.id.wortMix_txtfirstWorthLiters)).getText().toString());
			double firstWortGravity =Double.parseDouble(((EditText)findViewById(R.id.wortMix_txtfirstWortGravity)).getText().toString());
			double secondWortLiters =Double.parseDouble(((EditText)findViewById(R.id.wortMix_txtsecondWorthLiters)).getText().toString());
			double secondWortGravity =Double.parseDouble(((EditText)findViewById(R.id.wortMix_txtsecondWortGravity)).getText().toString());
		
		
			if(firstWortLiters == 0 || firstWortGravity == 0 || secondWortLiters == 0 || secondWortGravity == 0){
				isValid = false;
			}
		}
		catch(NumberFormatException e){
			isValid = false;
		}
		
		return isValid;
	}
	

	public void btnClean_Click(View v) {
		clearFields();
	}
	public void btnCalc_Click(View v) {
		if(validateFields()){
			double firstWortLiters =Double.parseDouble(((EditText)findViewById(R.id.wortMix_txtfirstWorthLiters)).getText().toString());
			double firstWortGravity =Double.parseDouble(((EditText)findViewById(R.id.wortMix_txtfirstWortGravity)).getText().toString());
			double secondWortLiters =Double.parseDouble(((EditText)findViewById(R.id.wortMix_txtsecondWorthLiters)).getText().toString());
			double secondWortGravity =Double.parseDouble(((EditText)findViewById(R.id.wortMix_txtsecondWortGravity)).getText().toString());
			Calcs calcmgr = new Calcs();
			double resul = calcmgr.WortMix(firstWortLiters, firstWortGravity, secondWortLiters, secondWortGravity);
			
			TableLayout wTableResults = (TableLayout) findViewById(R.id.wortMix_Results);
			
			wTableResults.removeAllViews();
	
			String[] wHeaderArray= new String[3];
			wHeaderArray[0]="Resultado";
			wHeaderArray[1]="";
			wHeaderArray[2]="";
			
			// Encabezado
			wTableResults.addView(Utils.GetTableRow(staticWortMix,wHeaderArray,true));
			
			//Items
			String[] wItemsArray= new String[3];
			wItemsArray[0]="Densidad Resultante";
			wItemsArray[1]= String.format("%.2f",resul);
			wItemsArray[2]="";
			wTableResults.addView(Utils.GetTableRow(staticWortMix,wItemsArray,false));
			
			wTableResults.setStretchAllColumns(true);
			wTableResults.setShrinkAllColumns(true);
		}
		else{
			AlertDialog alertDialog = new AlertDialog.Builder(this).create();  
		    alertDialog.setTitle("Error");  
		    alertDialog.setMessage(getText(R.string.msgValidateFields).toString());    
		    alertDialog.setButton("OK", new DialogInterface.OnClickListener() {  
		      public void onClick(DialogInterface dialog, int which) {  
		        return;  
		    } }); 
			
			alertDialog.show();
		}
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
	    if (keyCode == KeyEvent.KEYCODE_BACK) {
	        clearFields();
	    }
	    return super.onKeyDown(keyCode, event);
	}
	
	@Override
	public void onStop() {
		super.onStop();
	}
		
	@Override
	public void onResume() {
		super.onResume();
	}
}
