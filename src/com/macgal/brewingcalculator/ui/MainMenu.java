package com.macgal.brewingcalculator.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.macgal.brewingcalculator.MenuOption;
import com.macgal.brewingcalculator.R;

public class MainMenu extends Activity {

	//private static int mLoginTypeActual = 0;
	private static MenuOption[] menuCollection;
	private ListView mOptionList;
	private static MainMenu mainMenuStatic; 
	
	/** Called when the activity is first created. */
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		this.setTitle(getText(R.string.app_name));
		setContentView(R.layout.main_menu);
		mainMenuStatic = this;
		InitActivity();
	}
	
	private void InitActivity() {
	    setMenuOptions();
		mOptionList = (ListView) findViewById(R.id.MenuList);
		mOptionList.setAdapter(new MenuListItemAdapter(this, menuCollection));
	}
	
	
	// Clase para manejar los items del listview!!!
	private static class MenuListItemAdapter extends BaseAdapter {
		private LayoutInflater mInflater;
		private MenuOption[] menuCol;
		
		public MenuListItemAdapter(Context context, MenuOption[] pMenuCol) {
			mInflater = LayoutInflater.from(context);
			menuCol = pMenuCol;
			}

		public int getCount() {
			return menuCol.length;
		}

		public Object getItem(int position) {
			return position;
		}

		public long getItemId(int position) {
			return position;
		}

		public View getView(int position, View convertView, ViewGroup parent) {
			super.getItemViewType(position);
			if(menuCol[position] != null) {

			ViewHolder holder;
			if (convertView == null) {
				convertView = mInflater.inflate(R.layout.menu_item,	null);
				holder = new ViewHolder();
				holder.menuText = (TextView) convertView
						.findViewById(R.id.menu_txtName);
				holder.menuIcon = (ImageView) convertView
						.findViewById(R.id.menu_Icon);
				holder.btnViewDetail = (ImageView) convertView
						.findViewById(R.id.menu_GoDetail);

				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}

			// Menu Text
			holder.menuText.setText(menuCol[position]
					.getOptionText());
			holder.menuText.setClickable(true);
			holder.menuText.setTag(menuCol[position].getDestination());
			holder.menuText.setOnClickListener(new OnClickListener() {
				public void onClick(View view) {
					ShowDetail((Activity) view.getTag());
				}
			});

			// Button: ViewDetail
			holder.btnViewDetail.setClickable(true);
			holder.btnViewDetail.setTag(menuCol[position].getDestination());
			holder.btnViewDetail.setOnClickListener(new OnClickListener() {
				public void onClick(View view) {
					ShowDetail((Activity) view.getTag());
				}
			});

			// Button Icon
			holder.menuIcon.setImageDrawable(mainMenuStatic.getResources().getDrawable(menuCol[position].getIcon()));
			holder.menuIcon.setClickable(true);
			holder.menuIcon.setTag(menuCol[position].getDestination());
			holder.menuIcon.setOnClickListener(new OnClickListener() {
				public void onClick(View view) {
					ShowDetail((Activity) view.getTag());
				}
			});
			
			}
			else{
				convertView = new View(mainMenuStatic);
			}
				
			return convertView;
		}

		static class ViewHolder {
			TextView menuText;
			ImageView menuIcon;
			ImageView btnViewDetail;
		}

		}

	@Override
	public void onStop() {
		super.onStop();
	} 

	@Override
	public void onResume() {
		super.onResume();
	}
	
	private static void ShowDetail(Activity pActivity) {
		Intent myIntent = new Intent(mainMenuStatic, pActivity.getClass());
		mainMenuStatic.startActivity(myIntent);
	}

	private void setMenuOptions(){
		menuCollection = new MenuOption[9];
		
		MenuOption Tmp= new MenuOption();
		
		//Hydrometer Calibration
		Tmp= new MenuOption();
		Tmp.setOptionText(getText(R.string.MenuHydrometerCalibration).toString());
		Tmp.setIcon(R.drawable.logo);
		Tmp.setDestination(new HydrometerCalibration());
		Tmp.setId(R.string.MenuHydrometerCalibration);
		menuCollection[0]=Tmp;
		
		//Brix To SG
		Tmp= new MenuOption();
		Tmp.setOptionText(getText(R.string.MenuBrixConversion).toString());
		Tmp.setIcon(R.drawable.logo);
		Tmp.setDestination(new BrixConversion());
		Tmp.setId(R.string.MenuBrixConversion);
		menuCollection[1]=Tmp;
		
		//IBUs Calculations
		Tmp= new MenuOption();
		Tmp.setOptionText(getText(R.string.MenuIBU).toString());
		Tmp.setIcon(R.drawable.logo);
		Tmp.setDestination(new IBUs());
		Tmp.setId(R.string.MenuIBU);
		menuCollection[2]=Tmp;

		//Mash/Sparge Temperature
		Tmp= new MenuOption();
		Tmp.setOptionText(getText(R.string.MenuMashSparge).toString());
		Tmp.setIcon(R.drawable.logo);
		Tmp.setDestination(new MashSpargeTemp());
		Tmp.setId(R.string.MenuMashSparge);
		menuCollection[3]=Tmp;

		//Gravity Correction
		Tmp= new MenuOption();
		Tmp.setOptionText(getText(R.string.MenuGravityCorrection).toString());
		Tmp.setIcon(R.drawable.logo);
		Tmp.setDestination(new GravityCorrection());
		Tmp.setId(R.string.MenuGravityCorrection);
		menuCollection[4]=Tmp;
		
		//Wort Dilution
		Tmp= new MenuOption();
		Tmp.setOptionText(getText(R.string.MenuWorthDilution).toString());
		Tmp.setIcon(R.drawable.logo);
		Tmp.setDestination(new WortDilution());
		Tmp.setId(R.string.MenuWorthDilution);
		menuCollection[5]=Tmp;
		
		//Wort Mix
		Tmp= new MenuOption();
		Tmp.setOptionText(getText(R.string.MenuWortMix).toString());
		Tmp.setIcon(R.drawable.logo);
		Tmp.setDestination(new WortMix());
		Tmp.setId(R.string.MenuWortMix);
		menuCollection[6]=Tmp;
		
		//Attenuation
		Tmp= new MenuOption();
		Tmp.setOptionText(getText(R.string.MenuAttenuation).toString());
		Tmp.setIcon(R.drawable.logo);
		Tmp.setDestination(new Attenuation());
		Tmp.setId(R.string.MenuAttenuation);
		menuCollection[7]=Tmp;
		
		
		//Carbonation Level
		Tmp= new MenuOption();
		Tmp.setOptionText(getText(R.string.MenuCarbonationLevel).toString());
		Tmp.setIcon(R.drawable.logo);
		Tmp.setDestination(new CarbonationLevel());
		Tmp.setId(R.string.MenuCarbonationLevel);
		menuCollection[8]=Tmp;
		
		
	}
	
}
