/**
 * 
 */
package com.macgal.brewingcalculator.ui;

import com.macgal.brewingcalculator.Calcs;
import com.macgal.brewingcalculator.R;
import com.macgal.brewingcalculator.Utils;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.TableLayout;

/**
 * @author luchezco
 *
 */
public class WortDilution extends Activity {

	private static WortDilution staticWortDilution;
	
	/** Called when the activity is first created. */
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setTitle(getText(R.string.MenuWorthDilution));
		setContentView(R.layout.wortdilution_detail);
		staticWortDilution=this;
		clearFields();
	}
		
	private void clearFields(){
		((EditText)findViewById(R.id.wortDilution_txtoriginalWortVolume)).setText("");
		((EditText)findViewById(R.id.wortDilution_txtoriginalWortVolume)).requestFocus(MODE_PRIVATE);
		((EditText)findViewById(R.id.wortDilution_txtoriginalSpecificGravity)).setText("");
		((EditText)findViewById(R.id.wortDilution_txtoriginalIBU)).setText("");
		((EditText)findViewById(R.id.wortDilution_txtoriginalSRM)).setText("");
		((EditText)findViewById(R.id.wortDilution_txtwaterAdded)).setText("");
		
		TableLayout wTableResults = (TableLayout) findViewById(R.id.wortdilution_Results);
		wTableResults.removeAllViews();
	}
	
	private boolean validateFields()
	{
		boolean isValid=true;
		
		try{
			double originalWortVolume =Double.parseDouble(((EditText)findViewById(R.id.wortDilution_txtoriginalWortVolume)).getText().toString());
			double originalSpecificGravity =Double.parseDouble(((EditText)findViewById(R.id.wortDilution_txtoriginalSpecificGravity)).getText().toString());
			double originalIBU =Double.parseDouble(((EditText)findViewById(R.id.wortDilution_txtoriginalIBU)).getText().toString());
			double originalSRM =Double.parseDouble(((EditText)findViewById(R.id.wortDilution_txtoriginalSRM)).getText().toString());
			double waterAdded =Double.parseDouble(((EditText)findViewById(R.id.wortDilution_txtwaterAdded)).getText().toString());
		
			if(originalWortVolume == 0 || originalSpecificGravity == 0 ||  originalIBU == 0 || originalSRM == 0 || waterAdded == 0 ){
				isValid = false;
			}
		}
		catch(NumberFormatException e){
			isValid = false;
		}
		
		return isValid;
	}
	
	public void btnClean_Click(View v) {
		clearFields();
	}
	
	public void btnCalc_Click(View v) {
		if(validateFields()){
			double originalWortVolume =Double.parseDouble(((EditText)findViewById(R.id.wortDilution_txtoriginalWortVolume)).getText().toString());
			double originalSpecificGravity =Double.parseDouble(((EditText)findViewById(R.id.wortDilution_txtoriginalSpecificGravity)).getText().toString());
			double originalIBU =Double.parseDouble(((EditText)findViewById(R.id.wortDilution_txtoriginalIBU)).getText().toString());
			double originalSRM =Double.parseDouble(((EditText)findViewById(R.id.wortDilution_txtoriginalSRM)).getText().toString());
			double waterAdded =Double.parseDouble(((EditText)findViewById(R.id.wortDilution_txtwaterAdded)).getText().toString());
			
			Calcs calcmgr = new Calcs();
			double resul[] = calcmgr.WorthDilution(originalWortVolume, originalSpecificGravity, originalIBU, originalSRM, waterAdded);
			
			TableLayout wTableResults = (TableLayout) findViewById(R.id.wortdilution_Results);
			
			wTableResults.removeAllViews();
	
			String[] wHeaderArray= new String[3];
			wHeaderArray[0]="Resultado";
			wHeaderArray[1]="";
			wHeaderArray[2]="";
			
			// Encabezado
			wTableResults.addView(Utils.GetTableRow(staticWortDilution,wHeaderArray,true));
			
			//Items
			String[] wItemsArray= new String[3];
			wItemsArray[0]="Total de Mosto";
			wItemsArray[1]= String.format("%.2f",resul[0]);
			wItemsArray[2]=" lts";
			wTableResults.addView(Utils.GetTableRow(staticWortDilution,wItemsArray,false));
	
			wItemsArray[0]="Densidad Resultante";
			wItemsArray[1]= String.format("%.2f",resul[1]);
			wItemsArray[2]="";
			wTableResults.addView(Utils.GetTableRow(staticWortDilution,wItemsArray,false));
			
			wItemsArray[0]="IBUs Resultantes";
			wItemsArray[1]= String.format("%.2f",resul[2]);
			wItemsArray[2]="";
			wTableResults.addView(Utils.GetTableRow(staticWortDilution,wItemsArray,false));
			
			wItemsArray[0]="SRMs Resultantes";
			wItemsArray[1]= String.format("%.2f",resul[3]);
			wItemsArray[2]="";
			wTableResults.addView(Utils.GetTableRow(staticWortDilution,wItemsArray,false));
			
			wTableResults.setStretchAllColumns(true);
			wTableResults.setShrinkAllColumns(true);
		}
		else{
			AlertDialog alertDialog = new AlertDialog.Builder(this).create();  
		    alertDialog.setTitle("Error");  
		    alertDialog.setMessage(getText(R.string.msgValidateFields).toString());    
		    alertDialog.setButton("OK", new DialogInterface.OnClickListener() {  
		      public void onClick(DialogInterface dialog, int which) {  
		        return;  
		    } }); 
			
			alertDialog.show();
		}
		
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
	    if (keyCode == KeyEvent.KEYCODE_BACK) {
	        clearFields();
	    }
	    return super.onKeyDown(keyCode, event);
	}
	
	@Override
	public void onStop() {
		super.onStop();
	}
		
	@Override
	public void onResume() {
		super.onResume();
	}
}
