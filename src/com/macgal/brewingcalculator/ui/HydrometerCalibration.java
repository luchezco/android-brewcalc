/**
 * 
 */
package com.macgal.brewingcalculator.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.TableLayout;

import com.macgal.brewingcalculator.Calcs;
import com.macgal.brewingcalculator.R;
import com.macgal.brewingcalculator.Utils;

/**
 * @author luchezco
 *
 */
public class HydrometerCalibration extends Activity{
	
	private static HydrometerCalibration staticHydrometerCalibration;
	
	/** Called when the activity is first created. */
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setTitle(getText(R.string.MenuHydrometerCalibration));
		setContentView(R.layout.hydrometer_detail);
		staticHydrometerCalibration=this;
		clearFields();
	}
	
	
	private void clearFields(){
		((EditText)findViewById(R.id.hydrometer_txtSG)).setText("");
		((EditText)findViewById(R.id.hydrometer_txtSG)).requestFocus(MODE_PRIVATE);
		((EditText)findViewById(R.id.hydrometer_txtTemp)).setText("");
		
		TableLayout wTableResults = (TableLayout) findViewById(R.id.hydrometer__Results);
		wTableResults.removeAllViews();
	}
	
	private boolean validateFields()
	{
		boolean isValid=true;
		
		try{
			double originalGravity =Double.parseDouble(((EditText)findViewById(R.id.hydrometer_txtSG)).getText().toString());
			double temperature =Double.parseDouble(((EditText)findViewById(R.id.hydrometer_txtTemp)).getText().toString());
		
			if(originalGravity == 0 || temperature == 0 ){
				isValid = false;
			}
		}
		catch(NumberFormatException e){
			isValid = false;
		}
		
		return isValid;
	}
	
	public void btnClean_Click(View v) {
		clearFields();
	}
	
	
	public void btnCalc_Click(View v) {
		if(validateFields()){
			double originalGravity =Double.parseDouble(((EditText)findViewById(R.id.hydrometer_txtSG)).getText().toString());
			double temperature =Double.parseDouble(((EditText)findViewById(R.id.hydrometer_txtTemp)).getText().toString());
			Calcs calcmgr = new Calcs();
			double resul = calcmgr.HydrometerCorrection(temperature,originalGravity);
			
			TableLayout wTableResults = (TableLayout) findViewById(R.id.hydrometer__Results);
			
			wTableResults.removeAllViews();
	
			String[] wHeaderArray= new String[3];
			wHeaderArray[0]="Resultado";
			wHeaderArray[1]="";
			wHeaderArray[2]="";
			
			// Encabezado
			wTableResults.addView(Utils.GetTableRow(staticHydrometerCalibration,wHeaderArray,true));
			
			//Items
			String[] wItemsArray= new String[3];
			wItemsArray[0]="Densidad Corregida";
			wItemsArray[1]= String.format("%.2f",resul);
			wItemsArray[2]="";
			wTableResults.addView(Utils.GetTableRow(staticHydrometerCalibration,wItemsArray,false));
			
			wTableResults.setStretchAllColumns(true);
			wTableResults.setShrinkAllColumns(true);
		}
		else{
				AlertDialog alertDialog = new AlertDialog.Builder(this).create();  
			    alertDialog.setTitle("Error");  
			    alertDialog.setMessage(getText(R.string.msgValidateFields).toString());    
			    alertDialog.setButton("OK", new DialogInterface.OnClickListener() {  
			      public void onClick(DialogInterface dialog, int which) {  
			        return;  
			    } }); 
				
				alertDialog.show();
		}
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
	    if (keyCode == KeyEvent.KEYCODE_BACK) {
	        clearFields();
	    }
	    return super.onKeyDown(keyCode, event);
	}

	@Override
	public void onStop() {
		super.onStop();
	}
		
	@Override
	public void onResume() {
		super.onResume();
	}
}
