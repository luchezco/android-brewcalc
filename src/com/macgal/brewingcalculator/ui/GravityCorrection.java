/**
 * 
 */
package com.macgal.brewingcalculator.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.TableLayout;

import com.macgal.brewingcalculator.Calcs;
import com.macgal.brewingcalculator.R;
import com.macgal.brewingcalculator.Utils;

/**
 * @author luchezco
 *
 */
public class GravityCorrection extends Activity {

	private static GravityCorrection staticGravityCorrection;
	
	/** Called when the activity is first created. */
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setTitle(getText(R.string.MenuGravityCorrection));
		setContentView(R.layout.gravity_correction_detail);
		staticGravityCorrection=this;
		clearFields();
	}
		
	private void clearFields(){
		((EditText)findViewById(R.id.gravity_txtOrigSG)).setText("");
		((EditText)findViewById(R.id.gravity_txtOrigSG)).requestFocus(MODE_PRIVATE);
		((EditText)findViewById(R.id.gravity_txtDesiredSG)).setText("");
		((EditText)findViewById(R.id.gravity_txtWorthVolume)).setText("");
		
		TableLayout wTableResults = (TableLayout) findViewById(R.id.gravity_Results);
		wTableResults.removeAllViews();
	}
	
	private boolean validateFields()
	{
		boolean isValid=true;
		
		try{
			double originalGravity =Double.parseDouble(((EditText)findViewById(R.id.gravity_txtOrigSG)).getText().toString());
			double finalGravity =Double.parseDouble(((EditText)findViewById(R.id.gravity_txtDesiredSG)).getText().toString());
			double worthVolume =Double.parseDouble(((EditText)findViewById(R.id.gravity_txtWorthVolume)).getText().toString());
		
			if(originalGravity == 0 || finalGravity == 0 ||  worthVolume == 0){
				isValid = false;
			}
		}
		catch(NumberFormatException e){
			isValid = false;
		}
		
		return isValid;
	}

	
	public void btnClean_Click(View v) {
		clearFields();
	}
	
	public void btnCalc_Click(View v) {
		if(validateFields()){
			double originalGravity =Double.parseDouble(((EditText)findViewById(R.id.gravity_txtOrigSG)).getText().toString());
			double finalGravity =Double.parseDouble(((EditText)findViewById(R.id.gravity_txtDesiredSG)).getText().toString());
			double worthVolume =Double.parseDouble(((EditText)findViewById(R.id.gravity_txtWorthVolume)).getText().toString());
			
			Calcs calcmgr = new Calcs();
			double[] resul = calcmgr.GravityCorrection(originalGravity, finalGravity, worthVolume);
			
			TableLayout wTableResults = (TableLayout) findViewById(R.id.gravity_Results);
			
			wTableResults.removeAllViews();
	
			String[] wHeaderArray= new String[3];
			wHeaderArray[0]="Resultado";
			wHeaderArray[1]="";
			wHeaderArray[2]="";
			
			// Encabezado
			wTableResults.addView(Utils.GetTableRow(staticGravityCorrection,wHeaderArray,true));
			
			//Items
			String[] wItemsArray= new String[3];
			wItemsArray[0]="Az�car";
			wItemsArray[1]= String.format("%.2f",resul[0]);
			wItemsArray[2]=" grs";
			wTableResults.addView(Utils.GetTableRow(staticGravityCorrection,wItemsArray,false));
	
			wItemsArray[0]="Extracto de malta seco";
			wItemsArray[1]= String.format("%.2f",resul[1]);
			wItemsArray[2]=" grs";
			wTableResults.addView(Utils.GetTableRow(staticGravityCorrection,wItemsArray,false));
			
			wItemsArray[0]="Extracto de malta l�quido";
			wItemsArray[1]= String.format("%.2f",resul[2]);
			wItemsArray[2]=" grs";
			wTableResults.addView(Utils.GetTableRow(staticGravityCorrection,wItemsArray,false));
			
			wTableResults.setStretchAllColumns(true);
			wTableResults.setShrinkAllColumns(true);
			}
			else{
					AlertDialog alertDialog = new AlertDialog.Builder(this).create();  
				    alertDialog.setTitle("Error");  
				    alertDialog.setMessage(getText(R.string.msgValidateFields).toString());    
				    alertDialog.setButton("OK", new DialogInterface.OnClickListener() {  
				      public void onClick(DialogInterface dialog, int which) {  
				        return;  
				    } }); 
					
					alertDialog.show();
			}
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
	    if (keyCode == KeyEvent.KEYCODE_BACK) {
	        clearFields();
	    }
	    return super.onKeyDown(keyCode, event);
	}
	
	@Override
	public void onStop() {
		super.onStop();
	}
		
	@Override
	public void onResume() {
		super.onResume();
	}
	
}
