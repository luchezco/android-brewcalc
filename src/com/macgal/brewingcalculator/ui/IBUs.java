package com.macgal.brewingcalculator.ui;

import com.macgal.brewingcalculator.Calcs;
import com.macgal.brewingcalculator.R;
import com.macgal.brewingcalculator.Utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.TableLayout;

public class IBUs extends Activity {

	private static IBUs staticIBUs;
	
	/** Called when the activity is first created. */
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setTitle(getText(R.string.MenuIBU));
		setContentView(R.layout.ibus_detail);
		staticIBUs=this;
		clearFields();
	}
	
	private void clearFields(){
		((EditText)findViewById(R.id.ibuCalc_txtHopsAlphaAcidRate)).setText("");
		((EditText)findViewById(R.id.ibuCalc_txtHopsAlphaAcidRate)).requestFocus(MODE_PRIVATE);
		((EditText)findViewById(R.id.ibuCalc_txtHopGrams)).setText("");
		((EditText)findViewById(R.id.ibuCalc_txtTimeInMins)).setText("");
		((EditText)findViewById(R.id.ibuCalc_txtWorthLiters)).setText("");
		((EditText)findViewById(R.id.ibuCalc_txtWortGravity)).setText("");
		
		TableLayout wTableResults = (TableLayout) findViewById(R.id.ibuCalc_Results);
		wTableResults.removeAllViews();
	}
	
	private boolean validateFields()
	{
		boolean isValid=true;
		
		try{
			double hopsAlphaAcidRate =Double.parseDouble(((EditText)findViewById(R.id.ibuCalc_txtHopsAlphaAcidRate)).getText().toString());
			double hopGrams =Double.parseDouble(((EditText)findViewById(R.id.ibuCalc_txtHopGrams)).getText().toString());
			double timeInMins =Double.parseDouble(((EditText)findViewById(R.id.ibuCalc_txtTimeInMins)).getText().toString());
			double worthLiters =Double.parseDouble(((EditText)findViewById(R.id.ibuCalc_txtWorthLiters)).getText().toString());
			double wortGravity =Double.parseDouble(((EditText)findViewById(R.id.ibuCalc_txtWortGravity)).getText().toString());
		
			if(hopsAlphaAcidRate == 0 || hopGrams == 0 ||  timeInMins == 0 || worthLiters == 0 || wortGravity == 0 ){
				isValid = false;
			}
		}
		catch(NumberFormatException e){
			isValid = false;
		}
		
		return isValid;
	}
	
	public void btnClean_Click(View v) {
		clearFields();
	}

	
	public void btnCalc_Click(View v) {
		if(validateFields()){
			double hopsAlphaAcidRate =Double.parseDouble(((EditText)findViewById(R.id.ibuCalc_txtHopsAlphaAcidRate)).getText().toString());
			double hopGrams =Double.parseDouble(((EditText)findViewById(R.id.ibuCalc_txtHopGrams)).getText().toString());
			double timeInMins =Double.parseDouble(((EditText)findViewById(R.id.ibuCalc_txtTimeInMins)).getText().toString());
			double worthLiters =Double.parseDouble(((EditText)findViewById(R.id.ibuCalc_txtWorthLiters)).getText().toString());
			double wortGravity =Double.parseDouble(((EditText)findViewById(R.id.ibuCalc_txtWortGravity)).getText().toString());
		
			Calcs calcmgr = new Calcs();
			double resul = calcmgr.calculateIBUS(hopsAlphaAcidRate, hopGrams, worthLiters, wortGravity, timeInMins);
			
			TableLayout wTableResults = (TableLayout) findViewById(R.id.ibuCalc_Results);
			
			wTableResults.removeAllViews();
	
			String[] wHeaderArray= new String[3];
			wHeaderArray[0]="Resultado";
			wHeaderArray[1]="";
			wHeaderArray[2]="";
			
			// Encabezado
			wTableResults.addView(Utils.GetTableRow(staticIBUs,wHeaderArray,true));
			
			//Items
			String[] wItemsArray= new String[3];
			wItemsArray[0]="IBUs";
			wItemsArray[1]= String.format("%.2f",resul);
			wItemsArray[2]=" ";
			wTableResults.addView(Utils.GetTableRow(staticIBUs,wItemsArray,false));
	
			wTableResults.setStretchAllColumns(true);
			wTableResults.setShrinkAllColumns(true);
		}
		else{
			AlertDialog alertDialog = new AlertDialog.Builder(this).create();  
		    alertDialog.setTitle("Error");  
		    alertDialog.setMessage(getText(R.string.msgValidateFields).toString());    
		    alertDialog.setButton("OK", new DialogInterface.OnClickListener() {  
		      public void onClick(DialogInterface dialog, int which) {  
		        return;  
		    } }); 
			
			alertDialog.show();
		}
		
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
	    if (keyCode == KeyEvent.KEYCODE_BACK) {
	        clearFields();
	    }
	    return super.onKeyDown(keyCode, event);
	}
	
	@Override
	public void onStop() {
		super.onStop();
	}
		
	@Override
	public void onResume() {
		super.onResume();
	}
	
}
