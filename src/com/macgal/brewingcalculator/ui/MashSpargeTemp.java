/**
 * 
 */
package com.macgal.brewingcalculator.ui;

import com.macgal.brewingcalculator.Calcs;
import com.macgal.brewingcalculator.R;
import com.macgal.brewingcalculator.Utils;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.TableLayout;

/**
 * @author luchezco
 *
 */
public class MashSpargeTemp extends Activity {

	private static MashSpargeTemp staticMashSpargeTemp;
	
	/** Called when the activity is first created. */
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setTitle(getText(R.string.MenuMashSparge));
		setContentView(R.layout.mashsparge_detail);
		staticMashSpargeTemp=this;
		clearFields();
	}
	
	private void clearFields(){
		((EditText)findViewById(R.id.mashSparge_txtgrainVolume)).setText("");
		((EditText)findViewById(R.id.mashSparge_txtgrainVolume)).requestFocus(MODE_PRIVATE);
		((EditText)findViewById(R.id.mashSparge_txtgrainTemp)).setText("25");
		((EditText)findViewById(R.id.mashSparge_txtgrainAbsorbtion)).setText("0.9");
		((EditText)findViewById(R.id.mashSparge_txtwaterGrainRatio)).setText("");
		((EditText)findViewById(R.id.mashSparge_txtpercentWaterEvap)).setText("10");
		((EditText)findViewById(R.id.mashSparge_txtboilLength)).setText("");
		((EditText)findViewById(R.id.mashSparge_txtbatchSize)).setText("");
		((EditText)findViewById(R.id.mashSparge_txtdesiredSpargeTemp)).setText("");
		((EditText)findViewById(R.id.mashSparge_txtdesiredStrikeTemp)).setText("");
		
		TableLayout wTableResults = (TableLayout) findViewById(R.id.mashSparge_Results);
		wTableResults.removeAllViews();
	}
	
	private boolean validateFields()
	{
		boolean isValid=true;
		
		try{
			double grainVolume =Double.parseDouble(((EditText)findViewById(R.id.mashSparge_txtgrainVolume)).getText().toString());
			double grainTemp =Double.parseDouble(((EditText)findViewById(R.id.mashSparge_txtgrainTemp)).getText().toString());
			double grainAbsorbtion =Double.parseDouble(((EditText)findViewById(R.id.mashSparge_txtgrainAbsorbtion)).getText().toString());
			double waterGrainRatio =Double.parseDouble(((EditText)findViewById(R.id.mashSparge_txtwaterGrainRatio)).getText().toString());
			double percentWaterEvap =Double.parseDouble(((EditText)findViewById(R.id.mashSparge_txtpercentWaterEvap)).getText().toString());
			double boilLength =Double.parseDouble(((EditText)findViewById(R.id.mashSparge_txtboilLength)).getText().toString());
			double batchSize =Double.parseDouble(((EditText)findViewById(R.id.mashSparge_txtbatchSize)).getText().toString());
			double desiredSpargeTemp = Double.parseDouble(((EditText)findViewById(R.id.mashSparge_txtdesiredSpargeTemp)).getText().toString());
			double desiredStrikeTemp =Double.parseDouble(((EditText)findViewById(R.id.mashSparge_txtdesiredStrikeTemp)).getText().toString());
		
			if(grainVolume == 0 || grainTemp == 0 || grainAbsorbtion == 0 || waterGrainRatio == 0 || percentWaterEvap == 0 || 
			    boilLength == 0 || batchSize == 0 || desiredSpargeTemp == 0 || desiredStrikeTemp == 0){
				
				isValid = false;
			}
		}
		catch(NumberFormatException e){
			isValid = false;
		}
		
		return isValid;
	}
	
	
	public void btnClean_Click(View v) {
		clearFields();
	}

	
	public void btnCalc_Click(View v) {
		
		if(validateFields())
		{
			double grainVolume =Double.parseDouble(((EditText)findViewById(R.id.mashSparge_txtgrainVolume)).getText().toString());
			double grainTemp =Double.parseDouble(((EditText)findViewById(R.id.mashSparge_txtgrainTemp)).getText().toString());
			double grainAbsorbtion =Double.parseDouble(((EditText)findViewById(R.id.mashSparge_txtgrainAbsorbtion)).getText().toString());
			double waterGrainRatio =Double.parseDouble(((EditText)findViewById(R.id.mashSparge_txtwaterGrainRatio)).getText().toString());
			double percentWaterEvap =Double.parseDouble(((EditText)findViewById(R.id.mashSparge_txtpercentWaterEvap)).getText().toString());
			double boilLength =Double.parseDouble(((EditText)findViewById(R.id.mashSparge_txtboilLength)).getText().toString());
			double batchSize =Double.parseDouble(((EditText)findViewById(R.id.mashSparge_txtbatchSize)).getText().toString());
			double desiredSpargeTemp = Double.parseDouble(((EditText)findViewById(R.id.mashSparge_txtdesiredSpargeTemp)).getText().toString());
			double desiredStrikeTemp =Double.parseDouble(((EditText)findViewById(R.id.mashSparge_txtdesiredStrikeTemp)).getText().toString());
			
			Calcs calcmgr = new Calcs();
			double strikeWaterLiters = calcmgr.calculateStrikeWaterLiters(grainVolume, waterGrainRatio);
			double strikeWaterTemp = calcmgr.calculateStrikeWaterTemp(grainVolume, waterGrainRatio, desiredStrikeTemp, grainTemp);
			double spargeWaterLiters = calcmgr.calculateSpargeWaterLiters(grainVolume, waterGrainRatio, boilLength, percentWaterEvap, batchSize, grainAbsorbtion);
			double spargeWaterTemp = calcmgr.calculateSpargeWaterTemp(grainVolume, waterGrainRatio, desiredSpargeTemp, grainTemp, desiredStrikeTemp, boilLength, percentWaterEvap, batchSize, grainAbsorbtion);
			
			TableLayout wTableResults = (TableLayout) findViewById(R.id.mashSparge_Results);
			
			wTableResults.removeAllViews();
	
			String[] wHeaderArray= new String[3];
			wHeaderArray[0]="Resultado";
			wHeaderArray[1]="";
			wHeaderArray[2]="";
			
			// Encabezado
			wTableResults.addView(Utils.GetTableRow(staticMashSpargeTemp,wHeaderArray,true));
			
			//Items
			String[] wItemsArray= new String[3];
			wItemsArray[0]="Volumen de agua para el macerado";
			wItemsArray[1]= String.format("%.2f",strikeWaterLiters);
			wItemsArray[2]=" lts";
			wTableResults.addView(Utils.GetTableRow(staticMashSpargeTemp,wItemsArray,false));
	
			wItemsArray[0]="Temperatura del agua para el macerado";
			wItemsArray[1]= String.format("%.2f",strikeWaterTemp);
			wItemsArray[2]=" �C";
			wTableResults.addView(Utils.GetTableRow(staticMashSpargeTemp,wItemsArray,false));
			
			wItemsArray[0]="Volumen de agua para el lavado";
			wItemsArray[1]= String.format("%.2f",spargeWaterLiters);
			wItemsArray[2]=" lts";
			wTableResults.addView(Utils.GetTableRow(staticMashSpargeTemp,wItemsArray,false));
			
			wItemsArray[0]="Temperatura del agua para el lavado";
			wItemsArray[1]= String.format("%.2f",spargeWaterTemp);
			wItemsArray[2]=" �C";
			wTableResults.addView(Utils.GetTableRow(staticMashSpargeTemp,wItemsArray,false));
			
			wTableResults.setStretchAllColumns(true);
			wTableResults.setShrinkAllColumns(true);
		}
		else
		{
			AlertDialog alertDialog = new AlertDialog.Builder(this).create();  
		    alertDialog.setTitle("Error");  
		    alertDialog.setMessage(getText(R.string.msgValidateFields).toString());    
		    alertDialog.setButton("OK", new DialogInterface.OnClickListener() {  
		      public void onClick(DialogInterface dialog, int which) {  
		        return;  
		    } }); 
			
			alertDialog.show();
		}
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
	    if (keyCode == KeyEvent.KEYCODE_BACK) {
	        clearFields();
	    }
	    return super.onKeyDown(keyCode, event);
	}

	@Override
	public void onStop() {
		super.onStop();
	}
		
	@Override
	public void onResume() {
		super.onResume();
	}
}
