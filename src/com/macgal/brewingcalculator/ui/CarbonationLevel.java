package com.macgal.brewingcalculator.ui;

import com.macgal.brewingcalculator.Calcs;
import com.macgal.brewingcalculator.R;
import com.macgal.brewingcalculator.Utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.TableLayout;

public class CarbonationLevel extends Activity{
	
private static CarbonationLevel staticCarbonationLevel;
	
	/** Called when the activity is first created. */
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setTitle(getText(R.string.MenuCarbonationLevel));
		setContentView(R.layout.carbonationlevel_detail);
		staticCarbonationLevel=this;
		clearFields();
		}
	
	private void clearFields(){
		((EditText)findViewById(R.id.carbonationlevel_txtBatchSize)).setText("");
		((EditText)findViewById(R.id.carbonationlevel_txtBatchSize)).requestFocus(MODE_PRIVATE);
		((EditText)findViewById(R.id.carbonationlevel_txtBeerTemperature)).setText("");
		((EditText)findViewById(R.id.carbonationlevel_txtCarbonationLevel)).setText("");
		
		TableLayout wTableResults = (TableLayout) findViewById(R.id.carbonationlevel_Results);
		wTableResults.removeAllViews();
	}
	
	private boolean validateFields()
	{
		boolean isValid=true;
		
		try{
			double batchSize = Double.parseDouble(((EditText)findViewById(R.id.carbonationlevel_txtBatchSize)).getText().toString());
			double beerTemperature = Double.parseDouble(((EditText)findViewById(R.id.carbonationlevel_txtBeerTemperature)).getText().toString());
			double desiredCarbonationLevel = Double.parseDouble(((EditText)findViewById(R.id.carbonationlevel_txtCarbonationLevel)).getText().toString());
			
		
			if(batchSize == 0 || beerTemperature == 0 || desiredCarbonationLevel == 0){
				isValid = false;
			}
		}
		catch(NumberFormatException e){
			isValid = false;
		}
		
		return isValid;
	}
	

	public void btnClean_Click(View v) {
		clearFields();
	}

	public void btnCalc_Click(View v) {
		if(validateFields()){
			double batchSize = Double.parseDouble(((EditText)findViewById(R.id.carbonationlevel_txtBatchSize)).getText().toString());
			double beerTemperature = Double.parseDouble(((EditText)findViewById(R.id.carbonationlevel_txtBeerTemperature)).getText().toString());
			double desiredCarbonationLevel = Double.parseDouble(((EditText)findViewById(R.id.carbonationlevel_txtCarbonationLevel)).getText().toString());
		
			Calcs calcmgr = new Calcs();
			double[] resul = calcmgr.calculateCarbonationLevel(batchSize, beerTemperature, desiredCarbonationLevel);
			
			TableLayout wTableResults = (TableLayout) findViewById(R.id.carbonationlevel_Results);
			
			wTableResults.removeAllViews();
	
			String[] wHeaderArray= new String[3];
			wHeaderArray[0]="Resultado";
			wHeaderArray[1]="";
			wHeaderArray[2]="";
			
			// Encabezado
			wTableResults.addView(Utils.GetTableRow(staticCarbonationLevel,wHeaderArray,true));
			
			//Items
			String[] wItemsArray= new String[3];
			wItemsArray[0]="Az�car de ma�z";
			wItemsArray[1]= String.format("%.2f",resul[0]);
			wItemsArray[2]=" grs";
			wTableResults.addView(Utils.GetTableRow(staticCarbonationLevel,wItemsArray,false));
	
			wItemsArray[0]="Presi�n CO2";
			wItemsArray[1]= String.format("%.2f",resul[1]);
			wItemsArray[2]=" PSI";
			wTableResults.addView(Utils.GetTableRow(staticCarbonationLevel,wItemsArray,false));
			
			wItemsArray[0]="Presi�n CO2";
			wItemsArray[1]= String.format("%.2f",resul[2]);
			wItemsArray[2]=" KPA";
			wTableResults.addView(Utils.GetTableRow(staticCarbonationLevel,wItemsArray,false));
			
			wItemsArray[0]="Presi�n CO2";
			wItemsArray[1]= String.format("%.2f",resul[3]);
			wItemsArray[2]=" BARS";
			wTableResults.addView(Utils.GetTableRow(staticCarbonationLevel,wItemsArray,false));
			
			wTableResults.setStretchAllColumns(true);
			wTableResults.setShrinkAllColumns(true);
			}
			else{
					AlertDialog alertDialog = new AlertDialog.Builder(this).create();  
				    alertDialog.setTitle("Error");  
				    alertDialog.setMessage(getText(R.string.msgValidateFields).toString());    
				    alertDialog.setButton("OK", new DialogInterface.OnClickListener() {  
				      public void onClick(DialogInterface dialog, int which) {  
				        return;  
				    } }); 
					
					alertDialog.show();
			}
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
	    if (keyCode == KeyEvent.KEYCODE_BACK) {
	        clearFields();
	    }
	    return super.onKeyDown(keyCode, event);
	}
	
	@Override
	public void onStop() {
		super.onStop();
	}
		
	@Override
	public void onResume() {
		super.onResume();
	}
		
}
