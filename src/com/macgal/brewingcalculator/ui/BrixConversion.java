package com.macgal.brewingcalculator.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.TableLayout;

import com.macgal.brewingcalculator.Calcs;
import com.macgal.brewingcalculator.R;
import com.macgal.brewingcalculator.Utils;

public class BrixConversion extends Activity {

private static BrixConversion staticBrixConversion;
	
	/** Called when the activity is first created. */
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setTitle(getText(R.string.MenuBrixConversion));
		setContentView(R.layout.brixconversion_detail);
		staticBrixConversion=this;
		clearFields();
		}
	
	private void clearFields(){
		((EditText)findViewById(R.id.brixconversion_txtOB)).setText("");
		((EditText)findViewById(R.id.brixconversion_txtOB)).requestFocus(MODE_PRIVATE);
		((EditText)findViewById(R.id.brixconversion_txtCorrection)).setText("1");
		
		TableLayout wTableResults = (TableLayout) findViewById(R.id.brixconversion_Results);
		wTableResults.removeAllViews();
	}
	
	private boolean validateFields()
	{
		boolean isValid=true;
		
		try{
			double originalBrix =Double.parseDouble(((EditText)findViewById(R.id.brixconversion_txtOB)).getText().toString());
			double correctionFactor =Double.parseDouble(((EditText)findViewById(R.id.brixconversion_txtCorrection)).getText().toString());
			
			if(originalBrix == 0 || correctionFactor==0){
				isValid = false;
			}
		}
		catch(NumberFormatException e){
			isValid = false;
		}
		
		return isValid;
	}
	
	public void btnClean_Click(View v) {
		clearFields();
	}
	
	public void btnCalc_Click(View v) {
		if(validateFields()){
			double originalBrix =Double.parseDouble(((EditText)findViewById(R.id.brixconversion_txtOB)).getText().toString());
			double correctionFactor =Double.parseDouble(((EditText)findViewById(R.id.brixconversion_txtCorrection)).getText().toString());
			Calcs calcmgr = new Calcs();
			double resul = calcmgr.calculateBrixToSG(originalBrix, correctionFactor);
			
			TableLayout wTableResults = (TableLayout) findViewById(R.id.brixconversion_Results);
			
			wTableResults.removeAllViews();
	
			String[] wHeaderArray= new String[2];
			wHeaderArray[0]="Resultado";
			wHeaderArray[1]="";
			
			// Encabezado
			wTableResults.addView(Utils.GetTableRow(staticBrixConversion,wHeaderArray,true));
			
			//Items
			String[] wItemsArray= new String[3];
			wItemsArray[0]="Densidad";
			wItemsArray[1]= String.format("%.2f", resul);
			
			wTableResults.addView(Utils.GetTableRow(staticBrixConversion,wItemsArray,false));
			wTableResults.setStretchAllColumns(true);
			wTableResults.setShrinkAllColumns(true);
		}
		else{
			AlertDialog alertDialog = new AlertDialog.Builder(this).create();  
		    alertDialog.setTitle("Error");  
		    alertDialog.setMessage(getText(R.string.msgValidateFields).toString());    
		    alertDialog.setButton("OK", new DialogInterface.OnClickListener() {  
		      public void onClick(DialogInterface dialog, int which) {  
		        return;  
		    } }); 
			
			alertDialog.show();
		}
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
	    if (keyCode == KeyEvent.KEYCODE_BACK) {
	        clearFields();
	    }
	    return super.onKeyDown(keyCode, event);
	}
	
	@Override
	public void onStop() {
		super.onStop();
	}
		
	@Override
	public void onResume() {
		super.onResume();
	}
		
	
}
