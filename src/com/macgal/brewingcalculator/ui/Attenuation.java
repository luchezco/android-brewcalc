/**
 * 
 */
package com.macgal.brewingcalculator.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.TableLayout;

import com.macgal.brewingcalculator.Calcs;
import com.macgal.brewingcalculator.R;
import com.macgal.brewingcalculator.Utils;

/**
 * @author luchezco
 *
 */
public class Attenuation extends Activity {
	
	private static Attenuation staticAttenuation;
	
	/** Called when the activity is first created. */
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setTitle(getText(R.string.MenuAttenuation));
		setContentView(R.layout.attenuation_detail);
		staticAttenuation=this;
		clearFields();
		
		}
	
	private void clearFields(){
		((EditText)findViewById(R.id.attenuation_txtOG)).setText("");
		((EditText)findViewById(R.id.attenuation_txtOG)).requestFocus(MODE_PRIVATE);
		((EditText)findViewById(R.id.attenuation_txtFG)).setText("");
		
		TableLayout wTableResults = (TableLayout) findViewById(R.id.attenuation_Results);
		wTableResults.removeAllViews();
	}
	
	private boolean validateFields()
	{
		boolean isValid=true;
		
		try{
			double originalGravity =Double.parseDouble(((EditText)findViewById(R.id.attenuation_txtOG)).getText().toString());
			double finalGravity =Double.parseDouble(((EditText)findViewById(R.id.attenuation_txtFG)).getText().toString());
		
		
			if(originalGravity == 0 || finalGravity == 0){
				isValid = false;
			}
		}
		catch(NumberFormatException e){
			isValid = false;
		}
		
		return isValid;
	}

	
	public void btnClean_Click(View v) {
		clearFields();
	}
	
	public void btnCalc_Click(View v) {
		if(validateFields()){
			double originalGravity =Double.parseDouble(((EditText)findViewById(R.id.attenuation_txtOG)).getText().toString());
			double finalGravity =Double.parseDouble(((EditText)findViewById(R.id.attenuation_txtFG)).getText().toString());
			Calcs calcmgr = new Calcs();
			double[] resul = calcmgr.AlcoholAndYeastAttenuation(originalGravity, finalGravity);
			
			TableLayout wTableResults = (TableLayout) findViewById(R.id.attenuation_Results);
			
			wTableResults.removeAllViews();
	
			String[] wHeaderArray= new String[3];
			wHeaderArray[0]="Resultado";
			wHeaderArray[1]="";
			wHeaderArray[2]="";
			
			// Encabezado
			wTableResults.addView(Utils.GetTableRow(staticAttenuation,wHeaderArray,true));
			
			//Items
			String[] wItemsArray= new String[3];
			wItemsArray[0]="Alcohol por Volumen (ABV)";
			wItemsArray[1]= String.format("%.2f", resul[0]);
			wItemsArray[2]=" %";
			wTableResults.addView(Utils.GetTableRow(staticAttenuation,wItemsArray,false));
	
			wItemsArray[0]="Alcohol por Peso (ABW)";
			wItemsArray[1]= String.format("%.2f", resul[1]);
			wItemsArray[2]=" %";
			wTableResults.addView(Utils.GetTableRow(staticAttenuation,wItemsArray,false));
			
			wItemsArray[0]="Atenuación Aparente";
			wItemsArray[1]= String.format("%.2f", resul[2]);
			wItemsArray[2]=" %";
			wTableResults.addView(Utils.GetTableRow(staticAttenuation,wItemsArray,false));
			
			wItemsArray[0]="Atenuación Real";
			wItemsArray[1]= String.format("%.2f", resul[3]);
			wItemsArray[2]=" %";
			wTableResults.addView(Utils.GetTableRow(staticAttenuation,wItemsArray,false));
			
			wTableResults.setStretchAllColumns(true);
			wTableResults.setShrinkAllColumns(true);
		}
		else{
			AlertDialog alertDialog = new AlertDialog.Builder(this).create();  
		    alertDialog.setTitle("Error");  
		    alertDialog.setMessage(getText(R.string.msgValidateFields).toString());    
		    alertDialog.setButton("OK", new DialogInterface.OnClickListener() {  
		      public void onClick(DialogInterface dialog, int which) {  
		        return;  
		    } }); 
			
			alertDialog.show();
		}
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
	    if (keyCode == KeyEvent.KEYCODE_BACK) {
	        clearFields();
	    }
	    return super.onKeyDown(keyCode, event);
	}
	
	@Override
	public void onStop() {
		super.onStop();
	}
		
	@Override
	public void onResume() {
		super.onResume();
	}
		
}
