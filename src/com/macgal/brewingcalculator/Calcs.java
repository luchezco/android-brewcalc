/**
 * 
 */
package com.macgal.brewingcalculator;

/**
 * @author luchezco
 *
 *
 */
public class Calcs {
	
	/**
	 * Correcci�n de dens�metro (Hydrometer Temp. Calibration)
	 * @param temperature
	 * @param gravity
	 * @return
	 */
	public double HydrometerCorrection(double temperature, double gravity)
	{
		gravity = gravity/1000;
        double result=0;
        temperature =  ((temperature * 1.8) + 32.0);
        double tempPow2 = Math.pow(temperature, 2.0);
        double tempPow3 = Math.pow(temperature, 3.0);
        double auxResult = (1.313454 - (0.132674 * temperature)) + ((0.002057793 * tempPow2) - (2.627634E-06 * tempPow3));
        result = (gravity + (auxResult * 0.001));
        result = result * 1000;
        return result;
	}
	
	/**
	 * Mezcla de mostos (Wort Mix)
	 * @param first Worth Liters
	 * @param first Wort Gravity
	 * @param second Worth Liters
	 * @param second Wort Gravity
	 * @return
	 */
	public double WortMix(double firstWortLiters, double firstWortGravity, double secondWortLiters, double secondWortGravity)
	{
		 firstWortGravity = firstWortGravity/1000;
		 secondWortGravity = secondWortGravity/1000;
		 double result = 0;
         result = ((firstWortLiters * firstWortGravity) + (secondWortLiters * secondWortGravity)) / ((firstWortLiters + secondWortLiters));
         result = result * 1000;
         return result;
		
	}
	
	/**
	 * Correcci�n de densidades (Gravity correction)
	 * @param original Specific Gravity
	 * @param desired Specific Gravity
	 * @param original wort volume
	 * @return
	 */
	public double[] GravityCorrection(double originalSG, double desiredSG, double originalwortvolume){
        
		originalSG = originalSG/1000;
		desiredSG = desiredSG/1000;
		originalwortvolume *= 0.264172052;
		
        desiredSG = (desiredSG - 1.0) * 1000.0;
        desiredSG *= originalwortvolume;
        originalSG = (originalSG - 1.0) * 1000.0;
        originalSG *= originalwortvolume;
        double sugar = (desiredSG - originalSG) / 46.0;
        double dme = (desiredSG - originalSG) / 44.0;
        double lme = (desiredSG - originalSG) / 36.0;
        
        sugar *= 453.59237;
        dme *= 453.59237;
        lme *= 453.59237;
        
        double[] result = new double[3];
        result[0]=sugar;// sugar / dextrose
        result[1]=dme; // dry malt extract
        result[2]=lme; // liquid malt extract
        
        return result;
        
	}
	
	/**
	 * Diluci�n de mosto (Wort Dilution)
	 * @param original Wort Volume
	 * @param original Specific Gravity
	 * @param original IBU
	 * @param original SRM
	 * @param water Added
	 * @return
	 */
	public double[] WorthDilution(double originalWortVolume, double originalSpecificGravity, double originalIBU, double originalSRM, double waterAdded)
	{
		originalSpecificGravity=originalSpecificGravity/1000;
	    double totalVolume = originalWortVolume + waterAdded;
	    double resultingSG = ((originalSpecificGravity - 1.0) * 1000.0) * ((originalWortVolume * 0.264172052) / (totalVolume * 0.264172052));
	    resultingSG = (resultingSG / 1000.0) + 1.0;
	    double resultingIBU = originalIBU * ((originalWortVolume * 0.264172052) / (totalVolume * 0.264172052));
	    double resultingSRM = originalSRM * ((originalWortVolume * 0.264172052) / (totalVolume * 0.264172052));
	    resultingSG = resultingSG*1000;
	    
	    double[] result = new double[4];
	    result[0] = totalVolume;//Total Wort Volume
	    result[1] = resultingSG;//Resulting Specific Gravity
	    result[2] = resultingIBU;//Resulting IBUs
	    result[3] = resultingSRM;//Resulting SRMs
	    		
	    return result;
	}

	/**
	 * % Alcohol y Atenuaci�n (Alcohol % / Yeast Attenuation)
	 * @param original Gravity
	 * @param final Gravity
	 * @return
	 */
	public double[] AlcoholAndYeastAttenuation(double originalGravity, double finalGravity)
	{
		
		originalGravity=originalGravity/1000;
		finalGravity=finalGravity/1000;
        double ABV = (originalGravity - finalGravity) * 131.0;
        double ABW = 0.8 * ABV;
        double yeastAparentAttenuation = ((finalGravity - 1.0) * 1000.0) / ((originalGravity - 1.0) * 1000.0);
        yeastAparentAttenuation = (1.0 - yeastAparentAttenuation) * 100.0;
        double yeastRAaux1 = (-463.37 + (668.72 * originalGravity)) - (205.35 * Math.pow(originalGravity, 2.0));
        double yeastRAaux2 = (-463.37 + (668.72 * finalGravity)) - (205.35 * Math.pow(finalGravity, 2.0));
        double yeastRAaux3 = (0.1808 * yeastRAaux1) + (0.8192 * yeastRAaux2);
        double yeastRealAttenuation = yeastRAaux3 / yeastRAaux1;
        yeastRealAttenuation *= 100.0;
        yeastRealAttenuation = 100.0 - yeastRealAttenuation;
        
        double[] result = new double[4];
	    result[0] = ABV;//Alcohol by Volume
	    result[1] = ABW;//Alcohol by Weight
	    result[2] = yeastAparentAttenuation;//Yeast Aparent Attenuation
	    result[3] = yeastRealAttenuation;// Yeast Real Attenuation
	    		
	    return result;
	}

	
	/**
	 * Temperatura de Macerado
	 * @param grain
	 * @param water Grain Ratio
	 * @param desired Strike Temp
	 * @param grain Temp
	 * @return
	 */
	public double calculateStrikeWaterTemp(double grain, double waterGrainRatio, double desiredStrikeTemp,double grainTemp )
    {
        double strikeWaterTemp =((desiredStrikeTemp-grainTemp)*0.4/waterGrainRatio+desiredStrikeTemp);
        return strikeWaterTemp;
    }

    /**
     * Cantidad de Agua para el macerado
     * @param grain
     * @param water Grain Ratio
     * @return
     */
    public double calculateStrikeWaterLiters(double grain, double waterGrainRatio )
    {
        double liters = (double)grain * waterGrainRatio;
        return liters;
    }

     /**
      * Temperatura del agua de lavado
     * @param grain
     * @param water Grain Ratio
     * @param desired Sparge Temp
     * @param grain Temp
     * @param desired Strike Temp
     * @param boil Length
     * @param percent Water Evap
     * @param batch Size
     * @param Grain Absorbtion
     * @return
     */
    public double calculateSpargeWaterTemp(double grain, double waterGrainRatio, double desiredSpargeTemp, double grainTemp, double desiredStrikeTemp, double boilLength, double percentWaterEvap, double batchSize, double GrainAbsorbtion)
    {
        double liters = (double)grain * waterGrainRatio;
        double finalVolume = batchSize / 0.96;
        double wortAfterBoil = finalVolume / (1.0 - (((percentWaterEvap / 60.0) * boilLength) / 100.0));
        double spargeLiters = (((wortAfterBoil - liters) + (GrainAbsorbtion * grain)));
        double spargeWaterTemp = ((((spargeLiters * desiredSpargeTemp) - (((0.4 * grain) + liters) * (desiredStrikeTemp - desiredSpargeTemp))) / spargeLiters));
        return spargeWaterTemp;
    }

     /**
      * Cantidad de agua del lavado
     * @param grain
     * @param water Grain Ratio
     * @param boil Length
     * @param percent Water Evap
     * @param batch Size
     * @param Grain Absorbtion
     * @return
     */
    public double calculateSpargeWaterLiters(double grain, double waterGrainRatio, double boilLength, double percentWaterEvap, double batchSize, double GrainAbsorbtion)
     {
         double liters = (double)grain * waterGrainRatio;
         double finalVolume = batchSize / 0.96;
         double wortAfterBoil = finalVolume / (1.0 - (((percentWaterEvap / 60.0) * boilLength) / 100.0));
         double spargeLiters = (((wortAfterBoil - liters) + (GrainAbsorbtion * grain)));
         return spargeLiters;
     }
    
    
    /**
     * Calcula la cantidad de az�car o presi�n necesaria para carbonatar a un cierto nivel
     * @param batchSize
     * @param beer Temperature
     * @param carbonation Level
     * @return
     */
    public double[] calculateCarbonationLevel(double batchSize, double beerTemperature, double carbonationLevel){
    	 	
        beerTemperature = Utils.celsiusToFahrenheit(beerTemperature);
        batchSize = Utils.litersToGallons(batchSize);
        
        double primingSugarNeeded = (15.195 * batchSize) * (((carbonationLevel - 3.0378) + (0.050062 * beerTemperature)) - ((0.00026555 * beerTemperature) * beerTemperature));
        double psi = ((((-16.6999 - (0.0101059 * beerTemperature)) + (0.00116512 * Math.pow(beerTemperature, 2.0))) + ((0.173354 * beerTemperature) * carbonationLevel)) + (4.24267 * carbonationLevel)) - (0.0684226 * Math.pow(carbonationLevel, 2.0));
        double kpa = psi * 6.8948;
        double bars = psi * 0.0689475729;
        
        double[] result = new double[4];
	    result[0] = primingSugarNeeded;// Priming Sugar needed
	    result[1] = psi;// psi preassure needed
	    result[2] = kpa;// kpa preassure needed
	    result[3] = bars;// bars preassure needed
	    		
	    return result;
    }

    /**
     * Calcula la SG en base al valor en Brix y un factor de correcci�n del refract�metro
     * @param original Brix
     * @param correction Factor
     * @return
     */
    public double calculateBrixToSG(double originalBrix, double correctionFactor){
    	double result =( 0.999996 + 0.003855642 *originalBrix/correctionFactor + 0.000013695*Math.pow(originalBrix,2)/correctionFactor + 0.00000003739145*Math.pow(originalBrix,3)/correctionFactor)*1000;
    	return result;
    }
    
    
    /**
     * Calcula los IBUs aportados a la cerveza de acuerdo al algoritmo de Tinseth
     * @param %AA del lupulo
     * @param gramos de lupulo agregado
     * @param litros de mosto 
     * @param densidad del mosto
     * @param tiempo de hervos en el que se agrega el lupulo
     * @return
     */
    public double calculateIBUS(double hopsAlphaAcidRate, double hopGrams, double worthLiters, double wortGravity, double timeInMins){
    	
    	
    	wortGravity = wortGravity/1000;
    	double addedAlphaAcid = hopsAlphaAcidRate * hopGrams * 1000 / worthLiters;
    	double bignessFactor =   1.65 * Math.pow(0.000125,wortGravity - 1);
    	double boiltimeFactor =  (1 - Math.pow( Math.E,-0.04 * timeInMins))/4.15;
    	double res = bignessFactor * boiltimeFactor * addedAlphaAcid;
    	return res/100;
    			
    }
    
    
    
    
}
