package com.macgal.brewingcalculator;
import android.app.Activity;

public class MenuOption  {

	private String text;
	private int icon;
	private Activity destination;
	private int id;
	
	
	public String getOptionText() {
		return text;
	}
	public void setOptionText(String text) {
		this.text = text;
	}
	public int getIcon() {
		return icon;
	}
	public void setIcon(int icon) {
		this.icon = icon;
	}
	public Activity getDestination() {
		return destination;
	}
	public void setDestination(Activity destination) {
		this.destination = destination;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	
	
}
